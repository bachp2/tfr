#!/usr/bin/env python
#kudos to lfr :: https://answers.ros.org/question/237862/rosnode-kill/
import os
print("This is a python script not a ros\'s command\n")
nodes = os.popen("rosnode list").readlines()
for i in range(len(nodes)):
    nodes[i] = nodes[i].replace("\n","")

for node in nodes:
    os.system("rosnode kill "+ node)
