#!/usr/bin/env python

import os
import fnmatch

print("run this script after run roscore")
for root, dir, files in os.walk("../src"):
        for item in fnmatch.filter(files, "*.cpp"):
        	#print(os.path.join(root, item))
		root_path = str(root)
		item_path = str(item)
		pkg = root_path.split("/")[2]
		node = item_path.split(".")[0]
		os.system("rosrun "+ pkg + " " + node)
		#print("{} {}".format(pkg, node))
