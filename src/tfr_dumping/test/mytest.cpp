#include <gtest/gtest.h>
#include <ros/ros.h>
#include <stdio.h>
//can be shorten to #include "dumping_action_server.h" but I can't figure out how to do it in CMake, Collins actually did it for his utilities test
#include "../include/tfr_dumping/dumping_action_server.h"

std::string service_name;
double min_lin_vel, max_lin_vel, min_ang_vel, max_ang_vel, ang_tolerance;

TEST(DumpingTest, basicTest){
  	EXPECT_TRUE(true);
}

TEST(DumpingTest, variablesTest){
	EXPECT_TRUE(min_lin_vel!=0);
	EXPECT_TRUE(max_lin_vel!=0);
	EXPECT_TRUE(min_ang_vel!=0);
	EXPECT_TRUE(max_ang_vel!=0);
	EXPECT_TRUE(ang_tolerance!=0);
}

int main(int argc, char** argv){
  	ros::init(argc, argv, "dumping_action_server_test");
    ros::NodeHandle n;
    ros::param::param<double>("dumping_action_server/min_lin_vel",min_lin_vel, 0);
    ros::param::param<double>("dumping_action_server/max_lin_vel",max_lin_vel, 0);
    ros::param::param<double>("dumping_action_server/min_ang_vel",min_ang_vel, 0);
    ros::param::param<double>("dumping_action_server/max_ang_vel",max_ang_vel, 0);
    ros::param::param<double>("dumping_action_server/ang_tolerance",ang_tolerance, 0);
    ros::param::param<std::string>("dumping_action_server/image_service_name", service_name, "");
	//these lines below when executes, will wait for aruco and light detector(i.e. cameras), and will enter inf loop
	//I think of 2 options to procceed, define a TEST pre-processor guard and separate what the program behaves duing run-time with the robot 		and when testing
	//Second option, make some dummy objects (have to be convincing enough) to trick the ros backend to let us continue

    Dumper::DumpingConstraints constraints(min_lin_vel, max_lin_vel, min_ang_vel, max_ang_vel, ang_tolerance);
	ROS_INFO("Tesst");
    Dumper dumper(n, service_name, constraints);
    //ros::spin();
	//waits 5 seconds
  	sleep(5);
  	testing::InitGoogleTest(&argc, argv);
  	return RUN_ALL_TESTS();
}
