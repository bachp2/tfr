#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <tfr_msgs/EmptyAction.h>
#include <tfr_msgs/ArucoAction.h>
#include <tfr_msgs/WrappedImage.h>
#include <tfr_msgs/BinStateSrv.h>
#include <tfr_utilities/control_code.h>
#include <tfr_utilities/arm_manipulator.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <actionlib/server/simple_action_server.h>
#include <actionlib/client/simple_action_client.h>
#include "../include/tfr_dumping/dumping_action_server.h"
//this is a hack! Originally, there was no namespace definition but cmake keeps complaining about multiple mains redefinition; this fixes it.
namespace tfr_dumping
{
int main(int argc, char **argv)
{
    ros::init(argc, argv, "dumping_action_server");
    ros::NodeHandle n;
    double min_lin_vel, max_lin_vel, min_ang_vel, max_ang_vel, ang_tolerance;
    ros::param::param<double>("~min_lin_vel",min_lin_vel, 0);
    ros::param::param<double>("~max_lin_vel",max_lin_vel, 0);
    ros::param::param<double>("~min_ang_vel",min_ang_vel, 0);
    ros::param::param<double>("~max_ang_vel",max_ang_vel, 0);
    ros::param::param<double>("~ang_tolerance",ang_tolerance, 0);
    std::string service_name;
    ros::param::param<std::string>("~image_service_name", service_name, "");
    Dumper::DumpingConstraints constraints(min_lin_vel, max_lin_vel,
	    min_ang_vel, max_ang_vel, ang_tolerance);
    Dumper dumper(n, service_name, constraints);
    ros::spin();
    return 0;
}
}

