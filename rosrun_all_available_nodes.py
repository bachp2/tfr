#!/usr/bin/env python

import os
import subprocess
import fnmatch

print("run this script after run roscore and source ./devel/setup.bash")
subprocess.Popen("source ./devel/setup.bash", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
for root, dir, files in os.walk("./src"):
        for item in fnmatch.filter(files, "*.cpp"):
        	#print(os.path.join(root, item))
		root_path = str(root)
		item_path = str(item)
		pkg = root_path.split("/")[2]
		node = item_path.split(".")[0]
		subprocess.Popen("rosrun {} {}".format(pkg, node), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		#print("{} {}".format(pkg, node))
